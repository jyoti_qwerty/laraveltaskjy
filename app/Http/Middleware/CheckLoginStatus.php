<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\User;
use Session;

class CheckLoginStatus 
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!empty(Auth::user()->id)) {
            
            $check = User::where("id", Auth::user()->id)->where("status", 1)->first();
            
            if (!empty($check)) {
                return $next($request);
            }
        }
        Auth::logout();
        Session::flush();
        return redirect('/');
    }
}
