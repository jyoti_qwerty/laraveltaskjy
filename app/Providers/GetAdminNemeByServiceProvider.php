<?php

namespace App\Providers;
use App;
use Illuminate\Support\ServiceProvider;

class GetAdminNemeByServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('test',function() {
            return new \App\Test\AdminNameFacades;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
