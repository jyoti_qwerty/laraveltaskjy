<?php
   namespace App\Test;
   use Auth;
	use App\User;
	use Session;
   class AdminNameFacades{
      public function adminnamefacades() {
      	
      	if (!empty(Auth::user()->id)) {
            
            $userDetail = User::where("id", Auth::user()->id)->where("status", 1)->first();
            echo 'Welcome to '.$userDetail->name;
        }else{
        	echo 'Welcome to Admin';
        }
        
      }
   }
?>