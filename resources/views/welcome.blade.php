<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 margin-tb">
                            <div class="pull-left">
                                <h2 style="text-align: center;">{{$websitename}}</h2>
                                <h2>{{$heading}}</h2>
                            </div>
                            
                        </div>
                    </div>
                <br>
       
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
       
                <table class="table table-bordered" style="width:100%">
                    <tr>
                        <th>No</th>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Details</th>
                        
                    </tr>
                    @foreach ($products as $product)
                    <tr>
                        <td align="center">{{ ++$i }}</td>
                        <td align="center"><img src="{{url('/images/'.$product->image)}}" style="width:50px;height: 50px;"></td>
                        <td align="center">{{ $product->name }}</td>
                        <td align="center">{{ $product->detail }}</td>
                        
                    </tr>
                    @endforeach
                </table>

            </div>
        </div>
    </body>
</html>
